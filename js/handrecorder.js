$(document).on('pageinit', function()  {

    console.log("adding handrecorder page events");


    handrecorder.addCellEvents();


    $(".symbol").keyup(function () {
        $(this).val($(this).val().replace(/[d,D]/g, "♦").replace(/[h,H]/g, "♥").replace(/[c,C]/g, "♣").replace(/[s,S]/g, "♠"));
    });

    $('.selectMySeat td').on('click', function () {
        $(this).addClass('active').siblings().removeClass('active');
        var id = $(this).attr('id');
        var column = handrecorder.getColumn(id);
        console.log("enabling column:" + column);
        handrecorder.enableColumn(column);
    });

    $("#addrowbtn").on("click", function () {
        var html = "<tr>";
        var lastRow = handrecorder.incrementLastRow();
        for (var column = 0; column < handrecorder.getColumns(); column++) {
            var cellId = 'cellr' + lastRow + 'c' + column;
            html = html + '<td><input id="' + cellId + '" type="text" pattern="[0-9]*"/></td>';
        }
        html += "</tr>";

        $('#myTable').append(html).trigger("create");
        handrecorder.addCellEvents();
    });

    $("[id^=head]").on("change", function () {

        var currentValueStr = $(this).val();
        if (currentValueStr === '') return;

        var id = $(this).attr('id');
        var column = id.substring(7);

        var oldSum = $("#sumc" + column).val();
        if (oldSum === '' && $(this).val() !== '') {
            $("#sumc" + column).val($(this).val());
        }

        var currentValueInt = parseInt(currentValueStr);
        console.log("current value int: " + currentValueInt);

        var sumOfColumn = 0;
        $("[id$=c" + column + "]").each(function () {
            var id = $(this).attr('id');
            if (id.substring(0, 4) !== 'head' && id.substring(0, 3) !== 'sum') {
                if ($(this).val() !== '')
                    sumOfColumn += parseInt($(this).val());
                console.log(sumOfColumn);


                var arrayOfColumns = [];
                arrayOfColumns.push(sumOfColumn);
                console.log(arrayOfColumns.length);
            }
        });
        $(this).val(currentValueInt - sumOfColumn);

    });

    $("#savebtn").on("click", function () {
        handrecorder.saveHands();
    });

    $('.caped').keyup(function () {
        var text = $(this).val();
        var newText = text.replace(/(.*)([akqjt])(.*)/, function (v, m1, m2, m3) {
            return m1 + m2.toUpperCase() + m3;
        });
//        newText = newText.replace(/(.*)(\w\w)/,function(v,m1,m2) { return m1+m2+" ";});
        $(this).val(newText);
    });

});

var handrecorder =
    (function () {

        var HAND = {};
        var LAST_ROW = 7;
        var COLUMNS = 9;
        var highlightColors = ['#FFB273', '#63ADD0', '#59EA3A', 'burlywood', 'grey'];
        var highlightClasses = ['redRow', 'blueRow', 'greenRow', 'yellowRow', 'grayRow'];
        var highlightIndex = 0;
        var token, email, escapedEmail;


        function highlightRows(id) {
            var row = getRow(id);
            var column = getColumn(id);

            // abort if cell above is empty
            var valAbove = $('#cellr' + (row - 1) + 'c' + column).val();
            if (row == 1 || valAbove === '') return;

            console.log("row=" + row + ", row empty=" + isRowEmpty(row));

            // row much be empty and greater than 3
            if (!isRowEmpty(row) || row < 3) return;

            console.log("separate row is called row=" + row + ", column=" + column + " cell above: " + $('#cellr' + (row - 1) + 'c' + column).val());

            var isHighlighted = false;

            var startRow = -1;
            var endRow = -1;

            // determine row boundaries
            // find start row
            for (startRow = row - 1; startRow > 1; startRow--) {
                if (isCellHighlighted(startRow, column) || isRowEmpty(startRow)) {
                    ++startRow;
                    break;
                }
            }
            // find end row
            for (endRow = startRow; endRow <= LAST_ROW; endRow++) {
                if (isCellHighlighted(endRow, column) || isRowEmpty(endRow)) {
                    --endRow;
                    break;
                }
            }

            if (checkSumsInRowRange(startRow, endRow)) {
                highlightRowsInRange(startRow, endRow);
            }
        }

        function isRowEmpty(row) {
            for (var i = 0; i < COLUMNS; i++) {
                if ($('#cellr' + row + 'c' + i).val() !== '') {
                    return false;
                }
            }
            return true;
        }

// checks if the specified cell is highlighted
        function isCellHighlighted(row, column) {
            for (var i = 0; i < highlightColors.length; i++) {
                //console.log("class of parent: " + $('#cellr'+row+'c'+column).parent().hasClass("picker-formatted"));
                for (var i = 0; i < highlightColors.length; i++) {
                    if ($('#cellr' + row + 'c' + column).parent().hasClass("picker-formatted")) {
                        return true;
                    }
                }
            }
            return false;
        }

        function checkSumsInRowRange(startRow, endRow) {
            console.log("checkSumsInrow sr=" + startRow + ", er=" + endRow);
            if (endRow < startRow) {
                console.log("invalid row boundaries");
                return false;
            }

            var sums = {};
            for (var column = 0; column < COLUMNS; column++) {
                //console.log("disabled col="+column+", prop="+$('#cellr'+startRow+'c'+column).prop("disabled"));
                if ($('#cellr' + startRow + 'c' + column).prop("disabled") == 'true') {
                    sums[column] = -1;
                    continue;
                }

                var s = 0;
                for (var row = startRow; row <= endRow; row++) {
                    var cellValue = $('#cellr' + row + 'c' + column).val();
                    if (cellValue === 'c')
                        s += 0;
                    else if (isNumber(cellValue))
                        s += parseInt($('#cellr' + row + 'c' + column).val())
                }
                console.log('setting sum ' + s + ' for column ' + column);
                sums[column] = s;
            }

            // verify
            var oldColumn = 0;
            var numberOfSums = 0;
            var sumTracker = 0;
            var cols = {};
            for (var column = 0; column < COLUMNS; column++) {
                if (sums[column] < 1) continue;
                console.log("column=" + column + ", sum=" + sums[column]);
                if (sumTracker == 0)
                    sumTracker = sums[column];
                else if (sumTracker != sums[column]) return false;

            }
            return true;
        }

        function highlightRowsInRange(startRow, endRow) {
            console.log("highlighting rows: " + startRow + ", " + endRow);

            for (var row = startRow; row <= endRow; row++) {
                console.log("apply color " + highlightColors[highlightIndex] + " to row " + row);

                $("[id^='cellr" + row + "']").each(function () {
                    $(this).parent().css("background-color", highlightColors[highlightIndex]);
                    $(this).parent().addClass("picker-formatted");
                });
            }
            ++highlightIndex;

        }

        function isNumber(n) {
            if (n == '') return false;
            return !isNaN(parseFloat(n)) && isFinite(n);
        }

        function getRow(id) {
            var regex = /r(\d+)c(\d+)/g;
            var matches = regex.exec(id);
            return parseInt(matches[1]);

        }

        function getColumn(id) {
            var regex = /r(\d+)c(\d+)/g;
            var matches = regex.exec(id);
            return parseInt(matches[2]);
        }

        function calculatePotSize(tableId) {
            if (typeof tableId !== 'undefined') tableId += '_';
            else tableId = '';

            var potSize = 0;
            $("[id^=" + tableId + "cell]").each(function () {
                var value = $(this).val();
                if (value == '') value = $(this).html();
                if (value !== "" && value !== '&nbsp;' && !isNaN(value)) {
                    potSize += parseInt(value);
                }
            });
            var potId = tableId + "pot";

            $("#" + potId).html(potSize);

        }

        function getHeader(data, start, end) {
            var html = "<tr>";
            for (var i = start; i < end; i++) {
                data[i] = data[i].replace(/[d,D]/g, "♦").replace(/[h,H]/g, "♥").replace(/[c,C]/g, "♣").replace(/[s,S]/g, "♠");
                html += '<td>' + (data[i] || '&nbsp;') + '</td>\n';
            }
            html += "</tr>";
            return html;
        }

        function parseData(timestamp, data, start, end) {
            var html = "";
            var column = 0;
            var row = 0;
            for (var i = start; i < end; i++) {
                if (column == 0) {
                    html += '<tr>';
                }

                if (data[i] !== '') {
                    if (data[i] === '000') data[i] = 'X';
                    if (data[i] === '00') data[i] = 'F';
                    if (data[i] === '0') data[i] = 'C';
                }

                html += '<td style="background-color: ' + HAND.background[i] + ';" id="' + timestamp + '_cellr' + row + 'c' + column + '">' + (data[i] || '&nbsp;') + '</td>\n';
                ++column;
                if (column == COLUMNS) {
                    column = 0;
                    ++row;
                    html += '</tr>';
                }
            }
            return html;
        }

        function disableColumn(column) {
            $("[id^=headr0c" + column + "]").each(function () {
                $(this).prop("disabled", "true");
            });
            $("[id$=c" + column + "]").each(function () {
                $(this).prop("disabled", "true");
            });
        }

        function enableColumn(column) {
            $("[id^=headr0c" + column + "]").each(function () {
               $(this).removeProp("disabled");
            });
            $("[id$=c" + column + "]").each(function () {
                $(this).removeProp("disabled");
            });
        }


        function rgb2hex(rgb) {
            if (typeof rgb === 'undefined') return '';
            rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
            function hex(x) {
                return ("0" + parseInt(x).toString(16)).slice(-2);
            }

            return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
        }


        return {

            enableColumn: enableColumn,
            getColumn: getColumn,


            getEmail: function() {
                return localStorage.getItem("handrecorder:email");
            },

            getToken: function() {
                return localStorage.getItem("handrecorder:token");
            },



            incrementLastRow: function() {
                return ++LAST_ROW;
            },

            getLastRow: function() {
                return LAST_ROW;
            },

            setLastRow: function(lrow) {
                LAST_ROW = lrow;
            },

            getColumns: function() {
                return COLUMNS;
            },

            getParameterByName: function (name) {
                name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                    results = regex.exec(location.search);
                return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
            },

            // add cell event handlers
            addCellEvents: function () {
                $("[id^=cell]").on("change", function () {

                    var id = $(this).attr('id');
                    var currentValue = $(this).val();
                    var column = handrecorder.getColumn(id);

                    // special values to disable column
                    if (currentValue === "0" || currentValue === "00" || currentValue === "000") {
                        console.log("disabling column: " + column);

                        if (currentValue === "00")
                            $(this).val("F");
                        else if (currentValue === "0")
                            $(this).val("C");
                        else if (currentValue === "000")
                            $(this).val("X");

                        if (currentValue !== "0")
                            disableColumn(column);
                    }

                    // grab all rows to get the size of pot
                    calculatePotSize();

                    var topCellId = "headr0c" + column;

                    // update topCell
                    var topCellValue = $("#" + topCellId).val();
                    if (topCellValue !== "") {
                        var newValue = topCellValue - currentValue;
                        $("#" + topCellId).val(newValue);
                    }
                });
                $("[id^=cell]").on("focus", function () {
                    if ($(this).val() === '')
                        highlightRows($(this).attr('id'));
                });

            },

            saveHands: function () {
                var data = [];
                var background = [];
                $.each($('#data :text, textarea'), function (_, kv) {
                    if (kv.id.substr(0, 4) !== 'head') {
                        data.push(kv.value);
                        var style = $('#' + kv.id).parent().css("background-color");
                        console.log("style=" + rgb2hex(style));
                        background.push(rgb2hex(style));
                    }
                });
                var timestamp = new Date().getTime();
                HAND = {};
                HAND.board = data[0];
                HAND.header = data.slice(1, 19);
                HAND.data = data.slice(19, data.length - 1);
                HAND.note = data[data.length - 1];
                HAND.timestamp = timestamp;
                HAND.background = background.slice(19, background.length - 1);


                var escapedEmail = handrecorder.getEmail().replace(/\./g, ",");

                var ref = new Firebase('https://handrecorder.firebaseio.com/hands/' + escapedEmail + '/' + timestamp);
                ref.auth(handrecorder.getToken(), function (error) {
                    if (error) {
                        console.log("Login Failed!", error);
                        return;
                    } else {
                        console.log("Login Succeeded!");
                    }
                });
                ref.set(HAND, function (error) {
                    if (error) {
                        alert("Sync to Firebase failed");
                    }
                    else {
                        console.log("Saved to Firebase");
                        location.reload();
                    }
                });
            },

            restoreHands: function () {
                var handNumber = 0;

                var escapedEmail = handrecorder.getEmail().replace(/\./g, ",");
                console.log("Restoring from FireBase. email="+handrecorder.getEmail());

                var ref = new Firebase('https://handrecorder.firebaseio.com/hands/' + escapedEmail);
                ref.auth(handrecorder.getToken(), function (error) {
                    if (error) {
                        console.log("Login Failed!", error);
                        return;
                    } else {
                        console.log("Login Succeeded!");
                    }
                });

                ref.on('child_added', function (snapshot) {
                    ++handNumber;

                    // parse timestamp and display date
                    // parse data and display it
                    var timestamp = snapshot.name();
                    HAND = snapshot.val();

                    var fireDate = new Date(Number(timestamp));
                    var cleanFireDate = (fireDate.getMonth() + 1) + "/" + fireDate.getDate() + "/" + fireDate.getFullYear() + " " + fireDate.getHours() + ":" + fireDate.getMinutes();

                    var html = "";
                    html += "Hand #" + handNumber + " " + cleanFireDate;
                    html += '<table>\n';
                    HAND.board = HAND.board.replace(/[d,D]/g, "♦").replace(/[h,H]/g, "♥").replace(/[c,C]/g, "♣").replace(/[s,S]/g, "♠");

                    html += '<tr><td  colspan="9">' + 'Board:' + " " + HAND.board + ' ' + 'Pot Size:' + ' ' + '<span id="' + timestamp + '_pot"></span></td></tr>\n';
                    html += getHeader(HAND.header, 0, 9);
                    html += getHeader(HAND.header, 9, 18);
                    html += '<tr><td>SB</td><td>BB</td><td>UG</td><td>UG1</td><td>M1</td><td>M2</td><td>M3</td><td>CO</td><td>B</td></tr>';
                    html += parseData(timestamp, HAND.data, 0, (HAND.data.length - 1));
                    html += '</table>';
                    html += "<div>" + "Note:" + "<br/>" + HAND.note + "</div><br/>";

                    $("#hands").prepend(html);
                    calculatePotSize(timestamp);
                });
            }
        };
    }());














