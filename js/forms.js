$(document).on('pageinit', function() {
    $('#form').validate({
        rules: {
            email: {
                required: true
            },
            username: {
                required: true
            },
            password: {
                required: true
            }
        },
        messages: {
            email: {
                required: "Please enter your email."
            },
            username: {
                required: "Please enter your username."
            },
            password: {
                required: "Please enter your password."
            }
        },
        errorPlacement: function (error, element) {
            console.log("error: " + error);
            error.appendTo(element.parent().parent().after());
        },
        submitHandler: function (form) {
            submitForm();
            return false;
        }
    });

    function loading(showOrHide) {
        setTimeout(function(){
            $.mobile.loading(showOrHide);
        }, 1);
    }
    /* End of Validation */
    function submitForm() {
        // Send data to server through the ajax call
        // action is functionality we want to call and outputJSON is our data
        $.ajax({url: 'auth.php',
            data: {formData : $('#form').serialize()},
            type: 'post',
            async: 'true',
            dataType: 'json',
            beforeSend: function() {
                // This callback function will trigger before data is sent
                loading( "show");
            },
            complete: function() {
                // This callback function will trigger on data sent/received complete
                loading( "hide");
            },
            success: function (result) {
                if(result.token) {
                    localStorage.setItem("handrecorder:token", result.token);
                    localStorage.setItem("handrecorder:email", result.email);


                    document.location.href="addhand.html";;
                } else {
                    alert('Logon unsuccessful!');
                }
            },
            error: function (request,error) {
                console.log("error has occurred");
                // This callback function will trigger on unsuccessful action
                $("#show").show();


            }
        });

        return false; // cancel original event to prevent form submitting

    }

});