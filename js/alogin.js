$(document).on('pageinit', function() {

    var email = localStorage.getItem("handrecorder:email");
    var token = localStorage.getItem("handrecorder:token");
    if (typeof email !== 'undefined' && email !== null &&
        typeof token !== 'undefined' && token !== null) {
        document.location.href="addhand.html";
    }
});