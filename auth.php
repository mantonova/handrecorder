<?php

// Handles authentication and FireBase token generation

require_once 'lib/firebaseLib.php';
require_once 'lib/firebaseToken.php';


const DEFAULT_URL = 'https://handrecorder.firebaseio.com/';
const DEFAULT_TOKEN = 'OtNl3mtzOrIoptmmJPQ7vlVDSATlWd7WkFdLyjZm';
$firebase = new Firebase(DEFAULT_URL, DEFAULT_TOKEN);


//$formData = json_decode($_POST['formData']);

$params = array();
parse_str($_POST['formData'], $params);

$email = array_key_exists('email', $params) ? $params['email'] : null;
$password = array_key_exists('password', $params) ? $params['password'] : null;
$username = array_key_exists('username', $params) ? $params['username'] : null;
$type = array_key_exists('type', $params) ? $params['type'] : null;


if ($type == 'signup') {

    if (empty($email)) {
        echo '{"error":"Email is not specified"}';
        exit;
    }
    if (empty($password)) {
        echo '{"error":"Password is not set"}';
        exit;
    }

    $escapedEmail = escape_email($email);

    $response = $firebase->get('/users/'.$escapedEmail);
    if (! $response) {
        echo $response;
        echo '{"error":"Error creating user"}';
        exit;
    }

    $user =  array('username' => $username,
                  'password' => $password,
                  'email' => $email);

    //print "json=".json_encode($out);
    $response = $firebase->set('/users/'.$escapedEmail,$user);
    $responseObj = json_decode($response);


    if (isset($responseObj->error)) {
        http_response_code(401);
        echo '{"error":"Error signing up: "'.$responseObj->error.'}';
        exit;
    }
    else {
        $hands = array();
        $response = $firebase->set('/hands/'.$escapedEmail,$hands);
        $responseObj = json_decode($response);
        if (isset($responseObj->error)) {
            echo '{"error":"Error setting up Firebase: "'.$responseObj->error.'}';
            exit;
        }

        $tokenGen = new Services_FirebaseTokenGenerator(DEFAULT_TOKEN);
        $token = $tokenGen->createToken(array("id" => $escapedEmail));

        header('Content-type: application/json');
        http_response_code(200);
        echo '{"token":"'.$token.'","email":"'.$email.'"}';
    }

}
else {

    $emailEscaped = escape_email($email);

    if ($email == '') {
        echo '{"error":"Email empty"}';
        exit;
    }

    if ($email == '') {
        echo '{"error":"Email empty"}';
        exit;
    }


    $userObj = json_decode($firebase->get('/users/'.$emailEscaped));
    if (is_null($userObj)) {
        http_response_code(401);
        echo '{"error":"Failed to login"}';
        exit;
    }

    // TODO: compare given pass with stored
    if ($password != $userObj->{'password'}) {
        http_response_code(401);
        echo '{"error":"Password incorrect"}';
        exit;
    }

    $tokenGen = new Services_FirebaseTokenGenerator(DEFAULT_TOKEN);
    $token = $tokenGen->createToken(array("id" => $emailEscaped));

    header('Content-type: application/json');
    http_response_code(200);
    echo '{"token":"'.$token.'","email":"'.$email.'"}';

}

exit;


// signup: check if username already exists. if not, create new

// sign in: check if username exists and password hash matches



// $tokenGen = new Services_FirebaseTokenGenerator(DEFAULT_TOKEN);
// $token = $tokenGen->createToken(array("id" => "example"));


// $uri = "https://handrecorder.firebaseio.com/.json?auth=".$token;
// var_dump(file_get_contents($uri));


function escape_email($email) {
    if ($email === '') return;
    return preg_replace('/\./',',',$email);
}

function IsNullOrEmptyString($question){
    return (!isset($question) || trim($question)==='');
}

?>